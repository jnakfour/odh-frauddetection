# Fraud Detection Use Case

## Use Case

* Detect fraud credit card transactions
* Dataset used from [Kaggle](https://www.kaggle.com/isaikumar/creditcardfraud)
* Dataset includes time, amount and 28 hidden features to protect consumer data

## Goals

* Provide end-to-end AI/ML platform
* Create an AI/ML model that can predict fraud transactions
* Serve model and collect model metrics
* Provide monitoring tools for model and services used by DevOps
* Provide development tools for Data Scientists
* Provide ETL tools used by Data Engineers

## High Level Architecture

The following diagram describes a high level architectural view of the components used to create the Fraud Detection Use Case.
![Fraud Detection Architecture](FraudDetectionArch.png)

* Credit Card Transaction data is stored in Rook (Ceph).
* Data exploration and model development was performed in Jupyter notebook using Spark for ETL and SKlearn for exploring different model options.
* After selecting Random Forest Classifier model, it is trained on part of the full dataset and saved in Rook (ceph) as model.pkl.
* Seldon model extracts model.pkl from Rook (Ceph) and serves model. 
* To simulate Credit Card Transaction flow, Kafka consumer reads transaction data from Rook (Ceph) generates messages for Consumer
* Consumer reads messages posts HTTP requests on the seldon model interface to receive predictions for each transaction.
* Seldon model collects metrics on all transaction features and probability of a transaction being fraud. 
* Seldon also collects multiple metrics on request rate, http responses, etc,
* Spark and Kafka also collects metrics.
* Prometheus scrapes all the metrics, Grafana displays different metric time series graphs. 

